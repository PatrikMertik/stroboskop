# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://PatrikMertik@bitbucket.org/PatrikMertik/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/PatrikMertik/stroboskop/commits/6c2714dc7b54b13402895cc9675ca9c22561342b

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/PatrikMertik/stroboskop/commits/999d92bf7cc13d821be7bd935837dbe72c2dec7c

Naloga 6.3.2:
https://bitbucket.org/PatrikMertik/stroboskop/commits/83afe257830e462efa61df199fb28d921755d542

Naloga 6.3.3:
https://bitbucket.org/PatrikMertik/stroboskop/commits/f0dbe7adb2477bca9b3b309e13db7e199efdc5fc

Naloga 6.3.4:
https://bitbucket.org/PatrikMertik/stroboskop/commits/176adad470244a17169f77712cfcb4f6e605fc70

Naloga 6.3.5:

```
((UKAZI GIT))
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
((UVELJAVITEV))

Naloga 6.4.2:
((UVELJAVITEV))

Naloga 6.4.3:
((UVELJAVITEV))

Naloga 6.4.4:
((UVELJAVITEV))